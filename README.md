#Introduction

This is a toy script that attaches files recently added to a list of directories to an email. I use it to forward 
PDF copies of daily publications to my kindle so that I have fresh matrial to review every morning. I am aware that
forwards is not spelled forwords.

I am sure there are easier ways to acheive the same result. The primary reason for this project was to get familiar
with python's unit test libraries and TDD in general. The meat of the project is actually just a parser for the log
file, which I probably overbuilt.

#Useage
##Config
The config file is split into three major sections: directories, forwarding emails, and logging emails.


#Technical notes:
Where:

-   MaxFile is a non-negetive integer
-   MaxMagnitude is a non-negative integer
-   Dirs is an list (magnitude N) of lists (magnitude 3). Each entry of 
-   dirs is a list containing the key, absolute path, and regex for
-   each directory to be monitored by forworder
-   forwardEmails is a list of email addresses
-   logEmails is a list of email addresses

Requirements:

-   Fails if MaxFiles is negative
-   Fails if MaxFiles is not an integer
-   Uses first occurance if MaxFiles is not unique
-   Defaults to 0 if MaxFiles is not specified
-   Fails if MaxMag is negative
-   Fails if MaxMag is not an integer
-   Uses first occurance if MaxMag is not unique
-   Defaults to 0 if MaxMag is not specified
-   Fails if Directories is not specified
-   Fails if Directories is the trivial list
-   Uses first occurance of Directories is not unique
-   Fails if Forwarding Emails is not specified
-   Fails if Forwarding Emails is the trivial list
-   Uses first occurance of Logging Emails is not unique
-   Fails if Logging Emails is not specified
-   Fails if Logging Emails is the trivial list
-   Uses first occurance of Logging Emails is not unique
-   Ingnores malformed lines
