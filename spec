Config File

Options:
	MaxFiles
	MaxMagnitude
	Directories
	Forwarding Emails
	Logging Emails

MaxFiles
	Format:		MaxFiles NN
	Function:	Limits the number of files that can be forwarded to NN. NN must
				be a non-negative integer number. 0 denotes an unlimited number
				of files.  If excluded this option is set to the default value
				of 0.  
	Default:	0

MaxMagnitude
	Format:		MaxMagnitude NN
	Function:	Limits the total magnitude of the set of files to be forwarded
				to NN kilobites. NN must be a non-negative integer number.
				0 denotes no limit on the size of the files to be forwarded.
				If excluded this option is set to the default value of 0.
	Default:	0

Directories
	Format:		Directories
					Key1, Directory1, Regex1
					.
					.
					.
					KeyN, DirectoryN, RegexN
	Function:	Specifies the directories that forwords should monitor for
				content. KeyN is a user friendly handle to be used in logs
				and labeling. DirectoryN is the absolute path of the 
				directory to monitor. RegexN is a python regular expression
				denoting which files should be included in the forward.
	Default:	Required Explicitly

Forwarding Emails
	Format:		Forwarding Emails
					Email1
					.
					.
					.
					EmailN
	Function:	Specifies the email addresses forwords should send files to.
	Default:	Required Explicitly

Logging Emails
	Format:		Logging Emails
					Email1
					.
					.
					.
					EmailN
	Function:	Specifies the email addresses forwords should send logs to.
	Default:	Required Explicitly
