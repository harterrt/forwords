import os
from time import time
import fnmatch

def scan(dirs,maxFiles,maxMag):

    period = 24*60*60

    sendFiles = []
    overflow = []
    size = 0

    message = ""
    dirstring = "For directory {0}:\n"
    sendstring = "Included file : {0}.\n"
    magstring = "Refused to include file : {0} because maximum magnitude of {1} exceeded.\n"
    filestring = "Refused to include file : {0} because maximum file number of {1} exceeded.\n"

    for directory in dirs:
        dirName = directory[1]

        message = message + dirstring.format(directory)

        for fileName in os.listdir(dirName):
            filePath = os.path.normcase(dirName + '/' + fileName)

            #Check if file and if os.stat(filePath)[9] is within range
            # os.stat(filePath)[9] is ST_CTIME or creation time
            valid = (os.path.isfile(filePath) and
             (time() - period < os.stat(filePath)[9]) and
             fnmatch.fnmatch(fileName,directory[2]))

            if (valid):
                size = size + os.path.getsize(filePath)/1024
                if len(sendFiles)>maxFiles:
                    overflow.append(filePath)
                    message = message + filestring.format(fileName, maxFiles)
                elif size > maxMag:
                    overflow.append(filePath)
                    message = message + magstring.format(fileName, maxMag)
                else:
                    sendFiles.append(filePath)
                    message = message + sendstring.format(fileName)

    return sendFiles, overflow, message
