"""
Parses config file commands into a dict

returns dict of form:
{"MaxFile", "MaxMag", "Dirs", "forEmails", "logEmails"}

Where:
    MaxFile is a non-negetive integer
    MaxMagnitude is a non-negative integer
    Dirs is an list (magnitude N) of lists (magnitude 3). Each entry of 
        dirs is a list containing the key, absolute path, and regex for
        each directory to be monitored by forworder
    forwardEmails is a list of email addresses
    logEmails is a list of email addresses

Requirements:
    Fails if MaxFiles is negative
    Fails if MaxFiles is not an integer
    Uses first occurance if MaxFiles is not unique
    Defaults to 0 if MaxFiles is not specified
    Fails if MaxMag is negative
    Fails if MaxMag is not an integer
    Uses first occurance if MaxMag is not unique
    Defaults to 0 if MaxMag is not specified
    Fails if Directories is not specified
    Fails if Directories is the trivial list
    Uses first occurance of Directories is not unique
    Fails if Forwarding Emails is not specified
    Fails if Forwarding Emails is the trivial list
    Uses first occurance of Logging Emails is not unique
    Fails if Logging Emails is not specified
    Fails if Logging Emails is the trivial list
    Uses first occurance of Logging Emails is not unique
    Ingnores malformed lines

"""
import re

def read(sfile):
   #Specify data to be scrubbed from config file
    data = dict()

    #MaxMag and MaxFile
    data['maxMag'] = {'regex':'\s*MaxMagnitude\s*(-?\d*(?:\.?\d*))\s*', 
        'default':[0], 'type':int}
    data['maxFile'] = {'regex':'\s*MaxFiles\s*(-?\d*(?:\.?\d*))\s*', 
        'default':[0], 'type':int}

    #Directory
    dirDatum = '(?:[^\n,]+)'
    delim = '[\t ]*,[\t ]*'
    dirLine = delim.join(dirDatum for ii in range(3))
    data['dirs'] = {'regex':'\s*Directories\n'+
     '((?:[\t ]*' + dirLine + '\n)+)', 'type':str}

    #Forwarding and Logging Emails
    data['forEmails'] = {'regex':'\s*Forwarding Emails\n'+
     '((?:[\t ]*[^\s]+\n)+)', 'type':str}
    data['logEmails'] = {'regex':'\s*Logging Emails\n'+
     '((?:[\t ]*[^\s]+\n)+)', 'type':str}

   #Parse Away
    configText = open(sfile,'r').read()

    results = dict()
    for param in data.keys():
        matches = re.findall(data[param]['regex'], configText)

        if (len(matches)==0):
            #If no matches and no default, throw error. Else use default
            if 'default' not in data[param].keys():
                raise MalformedInput('Missing values for non-optional'+
                 ' parameter: '+param)
            else:
                results[param] = data[param]['default']
        else:
            results[param] = map(data[param]['type'],matches)

   #Verify input
    if results['maxMag'][0]<0:
        raise ValueError("MaxMagnitude must be positive. Got : "+
         str(results['maxMag'][0]))
    if results['maxFile'][0]<0:
        raise ValueError("MaxFiles must be positive. Got : "+
         str(results['maxFile'][0]))


   #Clean input
    results['maxMag'] = results['maxMag'][0]
    results['maxFile'] = results['maxFile'][0]
    results['dirs'] = [re.split(delim,line.strip()) 
     for line in results['dirs'][0].splitlines()]
    results['forEmails'] = map(str.strip,results['forEmails'][0].splitlines())
    results['logEmails'] = map(str.strip,results['logEmails'][0].splitlines())

    return results

class MalformedInput(Exception):
    pass
