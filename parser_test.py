import parser
import unittest
import os

testConfig = "__TestConfig__"

def prepMinConfigFile():
    """Prepares a Config file that barely meets standards"""
    minimalFile = """
    Directories
        key, ., *

    Forwarding Emails
        harterrt@gmail.com

    Logging Emails
        harterrt@gmail.com
    """
    config = open(testConfig,'w')
    config.write(minimalFile)
    config.close()


class TestInit(unittest.TestCase):
    def testMissingFile(self):
        """ Fails if file is not found """
        self.assertRaises(IOError,parser.read,testConfig+"asdfasdfasdfdd")


class TestOptionalParam(unittest.TestCase):
    def testMaxFileDefault(self): 
        """If unspecified MaxFiles is 0"""
        prepMinConfigFile()
        ans = parser.read(testConfig)

        self.assertEqual(0, ans['maxFile'])


    def testMaxFileValid(self):
        """Max Files is Non-Negative"""
        prepMinConfigFile()
        config = open(testConfig,'a')
        config.write('\nMaxFiles 25\n')
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(25, ans['maxFile'])


    def testMaxFileFloat(self):
        """Max Files is Non-Negative"""
        prepMinConfigFile()
        config = open(testConfig,'a')
        config.write('\nMaxFiles 25.1\n')
        config.close()

        self.assertRaises(ValueError, parser.read, testConfig)


    def testMaxFileZero(self):
        """Max Files is Non-Negative"""
        prepMinConfigFile()
        config = open(testConfig,'a')
        config.write('\nMaxFiles 0\n')
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(0, ans['maxFile'])


    def testMaxFileDouble(self):
        """Uses first Occurance if MaxFile is not unique"""
        prepMinConfigFile()
        config = open(testConfig,'a')
        config.write('\nMaxFiles 1\n')
        config.write('\nMaxFiles 2\n')
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(1, ans['maxFile'])


    def testMaxFileInvalid(self): 
        """Fails if MaxFiles is Negative"""
        prepMinConfigFile()
        config = open(testConfig,'a')
        config.write('\nMaxFiles -25\n')
        config.close()

        self.assertRaises(ValueError, parser.read, testConfig)


    def testMaxMagDefault(self): 
        """If unspecified MaxMag is 0"""
        prepMinConfigFile()
        ans = parser.read(testConfig)

        self.assertEqual(0, ans['maxMag'])


    def testMaxMagValid(self):
        """Max Magnitude is Non-Negative"""
        prepMinConfigFile()
        config = open(testConfig,'a')
        config.write('\nMaxMagnitude 25\n')
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(25, ans['maxMag'])


    def testMaxMagFloat(self):
        """Max Files is an Integer"""
        prepMinConfigFile()
        config = open(testConfig,'a')
        config.write('\nMaxFiles 25.1\n')
        config.close()

        self.assertRaises(ValueError, parser.read, testConfig)


    def testMaxMagZero(self):
        """Max Magnitude is Non-Negative"""
        prepMinConfigFile()
        config = open(testConfig,'a')
        config.write('\nMaxMagnitude 0\n')
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(0, ans['maxMag'])


    def testMaxMagDouble(self):
        """Uses first Occurance if MaxMag is not unique"""
        prepMinConfigFile()
        config = open(testConfig,'a')
        config.write('\nMaxMagnitude 1\n')
        config.write('\nMaxMagnitude 2\n')
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(1, ans['maxMag'])


    def testMaxMagInvalid(self): 
        """Fails if MaxMagnitude is Negative"""
        prepMinConfigFile()
        config = open(testConfig,'a')
        config.write('\nMaxMagnitude -25\n')
        config.close()

        self.assertRaises(ValueError, parser.read, testConfig)


def prepDirConfigFile():
    """Prepares a minimal config file with the Dir listing at the end"""

    dirFile = """
    Forwarding Emails
        harterrt@gmail.com

    Logging Emails
        harterrt@gmail.com

    Directories
        key, ., *
    """
    config = open(testConfig,'w')
    config.write(dirFile)
    config.close()


class TestDirParam(unittest.TestCase):
    def testDirMissing(self): 
        """Fails if Directories is not specified"""

        dirFile = """
        Forwarding Emails
            harterrt@gmail.com

        Logging Emails
            harterrt@gmail.com
        """
        config = open(testConfig,'w')
        config.write(dirFile)
        config.close()

        self.assertRaises(parser.MalformedInput,parser.read,testConfig)

    def testDirTrivial(self): 
        """Fails if Directories is the trivial list"""

        dirFile = """
        Forwarding Emails
            harterrt@gmail.com

        Logging Emails
            harterrt@gmail.com

        Directories
        """
        config = open(testConfig,'w')
        config.write(dirFile)
        config.close()

        self.assertRaises(parser.MalformedInput,parser.read,testConfig)

    def testValidInput(self): 
        """Testing valid minimal input"""

        prepDirConfigFile()

        ans = parser.read(testConfig)

        self.assertEqual(ans['dirs'],[['key','.','*']])

    def testManyDirs(self): 
        """Testing valid input with multiple dirs"""

        prepDirConfigFile()
        config = open(testConfig,'a')
        config.write("""\tkey2, /test/, *word*\n""")
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(ans['dirs'],[['key','.','*'],
         ['key2','/test/','*word*']])

    def testJunkDirs(self): 
        """Ignores malformed lines"""

        prepDirConfigFile()
        config = open(testConfig,'a')
        config.write("""\n\tkey2, ., *\n\tkey3, *\n\tkey3, ., *\n""")
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(ans['dirs'],[['key','.','*']])


def prepForEmailConfigFile():
    """Preps a minimalist config file with forwarding emails at the end"""

    forFile = """
    Logging Emails
        harterrt@gmail.com

    Directories
        key, ., *

    Forwarding Emails
        harterrt@gmail.com
    """
    config = open(testConfig,'w')
    config.write(forFile)
    config.close()


class TestForwardingEmails(unittest.TestCase):
    def testForEmailMissing(self):
        """Fails if Forwarding Emails is not specified"""

        forFile = """
        Logging Emails
            harterrt@gmail.com

        Directories
            key, ., *
        """
        config = open(testConfig,'w')
        config.write(forFile)
        config.close()

        self.assertRaises(parser.MalformedInput,parser.read,testConfig)


    def testForEmailTrivial(self):
        """Fails if Forwarding Email is the trivial list"""

        forFile = """
        Logging Emails
            harterrt@gmail.com

        Directories
            key, ., *

        Forwarding Emails
        """
        config = open(testConfig,'w')
        config.write(forFile)
        config.close()

        self.assertRaises(parser.MalformedInput,parser.read,testConfig)


    def testForEmailNotUnique(self):
        """Fails if Forwarding Email is not unique"""

        prepForEmailConfigFile()
        config = open(testConfig,'a')
        config.write("""\nForwarding Emails\n\tduplicate@aol.com\n""")
        config.close()

        ans = parser.read(testConfig)
        
        self.assertEqual(['harterrt@gmail.com'], ans['forEmails'])


    def testManyForEmails(self):
        """Testing valid input with multiple emails"""
        prepForEmailConfigFile()
        config = open(testConfig,'a')
        config.write("""\tduplicate@aol.com\n""")
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(['harterrt@gmail.com','duplicate@aol.com'],
            ans['forEmails'])


    def testOneForEmails(self):
        """Testing valid minimal input"""

        prepForEmailConfigFile()

        ans = parser.read(testConfig)

        self.assertEqual(['harterrt@gmail.com'],
            ans['forEmails'])


    def testJunkForEmails(self):
        """Ignores malformed lines"""
        prepForEmailConfigFile()
        config = open(testConfig,'a')
        config.write("""\n\tasdfiwer233232eavlj;com\n\tduplicate@aol.com""")
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(['harterrt@gmail.com'], ans['forEmails'])


def prepLogEmailConfigFile():
    """Preps minimal config file with logging emails at the end"""

    logFile = """
    Forwarding Emails
        harterrt@gmail.com

    Directories
        key, ., *

    Logging Emails
        harterrt@gmail.com
    """
    config = open(testConfig,'w')
    config.write(logFile)
    config.close()


class TestLogEmails(unittest.TestCase):
    def testLogEmailMissing(self):
        """Fails if Logging Emails is not specified"""

        logFile = """
        Forwarding Emails
            harterrt@gmail.com

        Directories
            key, ., *
        """
        config = open(testConfig,'w')
        config.write(logFile)
        config.close()

        self.assertRaises(parser.MalformedInput,parser.read,testConfig)


    def testLogEmailTrivial(self):
        """Fails if Logging Emails is the trivial list"""

        logFile = """
        Forwarding Emails
            harterrt@gmail.com

        Directories
            key, ., *

        Logging Emails
        """
        config = open(testConfig,'w')
        config.write(logFile)
        config.close()

        self.assertRaises(parser.MalformedInput,parser.read,testConfig)


    def testLogEmailNotUnique(self):
        """Fails if Logging Emails is not unique"""

        prepLogEmailConfigFile()
        config = open(testConfig,'a')
        config.write("""\nLogging Emails\n\tduplicate@aol.com\n""")
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(['harterrt@gmail.com'], ans['logEmails'])


    def testManyLogEmails(self):
        """Testing valid input with multiple email addresses"""

        prepLogEmailConfigFile()
        config = open(testConfig,'a')
        config.write("""\tduplicate@aol.com\n""")
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(['harterrt@gmail.com','duplicate@aol.com'],
            ans['logEmails'])


    def testOneLogEmails(self):
        """Testing valid minimal input"""

        prepLogEmailConfigFile()

        ans = parser.read(testConfig)

        self.assertEqual(['harterrt@gmail.com'], ans['logEmails'])


    def testJunkLogEmails(self):
        "Ignores malformed lines"""

        prepLogEmailConfigFile()
        config = open(testConfig,'a')
        config.write("""\n\tasdfiwer233232eavlj;com\n\tduplicate@aol.com""")
        config.close()

        ans = parser.read(testConfig)

        self.assertEqual(['harterrt@gmail.com'],
            ans['logEmails'])



if __name__ == "__main__":
        unittest.main()   
