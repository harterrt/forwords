
import parser
import reader
import transmitter
ii = 0

subject = "Forwords Update"
message = "-"*80 + "\n"
sendFiles = []

try:
    params = parser.read('config')
    sendFiles, overflow , tempMessage= reader.scan(params['dirs'],params['maxFile'],
        params['maxMag'])
    message = message + tempMessage

except Exception as e:
    message = message + str(e)

finally:
    message = message + "-"*80 + "\n"
    transmitter.send(params['forEmails'],params['logEmails'],
        subject, message, sendFiles)
