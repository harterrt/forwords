# Mail code ripped from:
# http://kutuma.blogspot.com/2007/08/sending-emails-via-gmail-with-python.html
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders
import os

def send(forEmails, logEmails, subject, message, sendFiles):

    for email in forEmails:
        mail(email, subject, message, sendFiles)

    for email in logEmails:
        mail(email, subject, message)


def mail(to, subject, text, attach=[]):
    """
    Creates and sends email as specified. Attach is a list of strings, each representing
    the path of a file to be attached to the email. 
    """
    gmail_user = "forwords.rth@gmail.com"
    gmail_pwd = "forwords"

    msg = MIMEMultipart()

    msg['From'] = gmail_user
    msg['To'] = to
    msg['Subject'] = subject

    msg.attach(MIMEText(text))

    for item in attach:
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(open(item, 'rb').read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition',
        'attachment; filename="%s"' % os.path.basename(item))
        msg.attach(part)

    mailServer = smtplib.SMTP("smtp.gmail.com", 587)
    mailServer.ehlo()
    mailServer.starttls()
    mailServer.ehlo()
    mailServer.login(gmail_user, gmail_pwd)
    mailServer.sendmail(gmail_user, to, msg.as_string())
    # Should be mailServer.quit(), but that crashes...
    mailServer.close()

# mail("harterrt+test@gmail.com", "Hello from python!",
#    "This is a email sent with python", ["MD_20110901.pdf","MD_20110901.pdf"])
